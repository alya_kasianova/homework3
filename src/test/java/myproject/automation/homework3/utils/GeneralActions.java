package myproject.automation.homework3.utils;

import com.google.common.base.Predicate;
import myproject.automation.homework3.pages.LoginPage;
import myproject.automation.homework3.pages.ProductData;
import myproject.automation.homework3.pages.ProductPage;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Contains main script actions that may be used in scripts.
 */
public class GeneralActions {
    private WebDriver driver;
    private WebDriverWait wait;

    public GeneralActions(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
    }

    /**
     * Logs in to Admin Panel.
     * @param login
     * @param password
     */
    public void login(String login, String password) {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.goToLoginPage();
        loginPage.fillEmailInput(login);
        loginPage.fillPasswordInput(password);
        loginPage.clickLoginBtn();
    }

    public void createProduct(ProductData newProduct) {
        ProductPage productPage = new ProductPage(driver, wait);
        productPage.openProductsPage();
        waitForContentLoad();
        productPage.clickOnAddNewProductBtn();
        waitForContentLoad();
        productPage.fillProductData(newProduct.getName(),
                                    String.valueOf(newProduct.getCount()),
                                    String.valueOf(newProduct.getPrice()));

        productPage.clickOnSwitch();
        productPage.waitUntilSuccsessTostWillBeVisible();
        productPage.closeTostMessage();
        productPage.saveProduct();
        productPage.waitUntilSuccsessTostWillBeVisible();
        productPage.closeTostMessage();
    }

    /**
     * Waits until page loader disappears from the page
     */
    public void waitForContentLoad() {
        wait.until((Predicate<WebDriver>) webDriver -> ((JavascriptExecutor) driver)
                .executeScript("return document.readyState").equals("complete"));
    }
}
