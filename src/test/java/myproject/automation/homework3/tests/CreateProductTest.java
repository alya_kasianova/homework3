package myproject.automation.homework3.tests;

import myproject.automation.homework3.pages.LoginPage;
import myproject.automation.homework3.pages.ProductData;
import myproject.automation.homework3.pages.ProductPage;
import myproject.automation.homework3.utils.BaseTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Random;
import java.util.UUID;

public class CreateProductTest extends BaseTest {
    ProductData productData = new ProductData(randomGUID(),
                                              integerRandomNumbers(),
                                              floatRandomNumbers());

    @DataProvider
    public Object[][] getLoginData() {
        return new String[][] {
                {"webinar.test@gmail.com", "Xcg7299bnSmMuRLp9ITw"}
        };
    }

    @Test(dataProvider = "getLoginData")
    public void createNewProduct(String login, String password) throws InterruptedException {
        actions.login(login, password);
        actions.waitForContentLoad();
        actions.createProduct(productData);
    }

    @Test(dependsOnMethods = { "createNewProduct" })
    public void validateVisibilityProductInTable(){
        LoginPage loginPage = new LoginPage(driver);
        loginPage.goToLoginPage();
        ProductPage productPage = new ProductPage(driver, wait);
        productPage.openProductsPage();
        productPage.openProduct(productData.getName());
        productPage.validateProductData(productData);
    }

    public int integerRandomNumbers(){
        return (int)(Math.random()*100);
    }

    public float floatRandomNumbers(){
        Random random = new Random();
        return (float) Math.round(random.nextInt(100_00) + 1) / 100;
    }

    public String randomGUID(){
        return UUID.randomUUID().toString();
    }
}
