package myproject.automation.homework3.pages;

import myproject.automation.homework3.utils.Helper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class ProductPage extends Helper{
    WebDriver driver;
    WebDriverWait wait;

    private By catalogMenuItem = By.xpath("//span[contains(text(), 'Каталог')]//ancestor::a");
    private By productsMenuItem = By.xpath("//a[contains(text(), 'товары')]");
    private By addNewProductBtn = By.id("page-header-desc-configuration-add");
    private By productNameInput = By.id("form_step1_name_1");
    private By productsCountInput = By.id("form_step1_qty_0_shortcut");
    private By productPriceInput = By.id("form_step1_price_shortcut");
    private By switchElement = By.className("switch-input");
    private By successTostMessage = By.xpath("//div[@class='growl-message' and text() = 'Настройки обновлены.']");
    private By closeTostbth = By.className("growl-close");
    private By saveProductBtn = By.cssSelector(".btn-primary.js-btn-save");

    public ProductPage(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
    }

    public void openProductsPage(){
        moveToElementUsingActions(driver, driver.findElement(catalogMenuItem));

        wait.until(ExpectedConditions.visibilityOfElementLocated(productsMenuItem));
        driver.findElement(productsMenuItem).click();
    }

    public void clickOnAddNewProductBtn(){
        driver.findElement(addNewProductBtn).click();
    }

    public void fillProductData(String productName, String productsCount, String productPrice){
        driver.findElement(productNameInput).sendKeys(productName);
        WebElement count = driver.findElement(productsCountInput);
        moveToElementUsingActions(driver, count);
        clearInputDataJavaScript(driver, count);
        count.sendKeys(productsCount);
        WebElement price = driver.findElement(productPriceInput);
        moveToElementUsingActions(driver, price);
        clearInputDataJavaScript(driver, price);
        price.sendKeys(productPrice);
    }

    public void clickOnSwitch(){
        driver.findElement(switchElement).click();
    }

    public void waitUntilSuccsessTostWillBeVisible(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(successTostMessage));
    }

    public void closeTostMessage(){
        driver.findElement(closeTostbth).click();
    }

    public  void saveProduct(){
        driver.findElement(saveProductBtn).submit();
    }

    public void openProduct(String productName){
        By product = By.xpath("//a[text() = '" + productName + "']");
        wait.until(ExpectedConditions.elementToBeClickable(product));
        clickOnElementJavaScript(driver, driver.findElement(product));
    }

    public void validateProductData(ProductData productData){
        wait.until(ExpectedConditions.elementToBeClickable(productNameInput));
        Assert.assertEquals(productData.getName(), driver.findElement(productNameInput).getAttribute("value"));
        WebElement count = driver.findElement(productsCountInput);
        moveToElementUsingActions(driver, count);
        Assert.assertEquals(productData.getCount(), count.getAttribute("value"));
        WebElement price = driver.findElement(productPriceInput);
        moveToElementUsingActions(driver, price);
        Assert.assertEquals(productData.getPrice(), price.getAttribute("value"));
    }
}
