package myproject.automation.homework3.pages;

public class ProductData {
    private String name;
    private int count;
    private float price;

    public ProductData(String name, int count, float price) {
        this.name = name;
        this.count = count;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public int getCount() {
        return count;
    }

    public float getPrice() {
        return price;
    }
}
